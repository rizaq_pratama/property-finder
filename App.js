/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
'use strict';
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import {createStackNavigator, createAppContainer} from 'react-navigation';

export class SearchPage extends Component {
  static navigationOptions = {
    title: 'Property Finder',
  };
  render() {
    return <Text style={styles.description}>Search for houses to buy!</Text>
  }
}

const App = createStackNavigator({
  Home: { screen: SearchPage },
});
export default createAppContainer(App);

const styles = StyleSheet.create({
  description: {
    fontSize: 18,
    textAlign: 'center',
    color: '#656565',
    marginTop: 65,
  },
});
